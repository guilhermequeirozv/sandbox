#include<iostream>
#include<string>
#include<vector>
#include <queue>

using namespace std;

class Node{
    private:
        void* value;
        bool visited;
        vector<Node*> childreen;
    public:
        Node(void *element){
            this->visited = false;
            this->value = element;
        }
        void setVisited(bool wasVisited){
            this->visited = wasVisited;
        }
        bool getVisited(){
            return this->visited;
        }
        int nElements(){
            return this->childreen.size();
        }
        Node* getChild(int i){
            return this->childreen.at(i);
        }
        void* getValue(){
            return this->value;
        }
        void addChild(Node* child){
            this->childreen.push_back(child);
        }
};

class Graph{
    private:
        vector<Node*> nodes;
    public:
        void insertIntoGraph(Node* node){
            nodes.push_back(node);
        }
        int getSize(){
            return nodes.size(); 
        }
        Node* getElement(int i){
            return this->nodes.at(i);
        }
};

Node* createNode(int value){
    int *pvalue = new int;
    (*pvalue) = value;
    Node* anode = new Node(pvalue);
    return anode;    
}

void dfs(Node *node){
    if (node == NULL) return;
    node->setVisited(true);
    cout << *(int*) node->getValue() << endl;
    for(int i = 0; i < node->nElements(); i++){
        if(!node->getChild(i)->getVisited()){
            dfs(node->getChild(i));
        }
    }
}

void bfs(Node* node){
    if (node == NULL) return;
    queue<Node*> myqueue;
    myqueue.push(node);
    while(!myqueue.empty()){
        Node * tnode = myqueue.front();
        cout << *(int*) tnode->getValue() << endl;
        tnode->setVisited(true);
        for(int i = 0; i < tnode->nElements(); i++){
            if(tnode->getChild(i)->getVisited()==false){
                myqueue.push(tnode->getChild(i));
            } 
        }         
        myqueue.pop();
    }    
}

int main(int argc, char** argv){
    Graph* mygraph = new Graph();

    for(int i = 0; i < 6; i++){
        Node *anode = createNode(i);
        mygraph->insertIntoGraph(anode);
    }
    
    mygraph->getElement(0)->addChild(mygraph->getElement(1));
    mygraph->getElement(0)->addChild(mygraph->getElement(4));
    mygraph->getElement(0)->addChild(mygraph->getElement(5));

    mygraph->getElement(1)->addChild(mygraph->getElement(3));
    mygraph->getElement(1)->addChild(mygraph->getElement(4));

    mygraph->getElement(2)->addChild(mygraph->getElement(1));

    mygraph->getElement(3)->addChild(mygraph->getElement(2));
    mygraph->getElement(3)->addChild(mygraph->getElement(4));

    //dfs(mygraph->getElement(0));
    //cout << "_" << endl;
    bfs(mygraph->getElement(0));

    return 1;
}