import boto3
import os


s3 = boto3.resource('s3')
bucket_name = input('Enter bucket name')
my_bucket = s3.Bucket(bucket_name)

for s3_object in my_bucket.objects.all():
	path, filename = os.path.split(s3_object.key)
    my_bucket.download_file(s3_object.key, filename)
