#!/usr/bin python3
"""mapper.py"""

import sys
import xml.etree.ElementTree as ET
import time

def handle_descriptor(descriptor, img_id, descriptor_name, tags):
	concat = []
	for i,tag in enumerate(tags):
		if descriptor.find(tag) is not None:
			elements = descriptor.find(tag).text.split()
			for element in elements:
				concat.append(int(element))
	return concat

with open("cophir1.txt") as file:
	for line in file:
		line = line.strip()
		mydir = '/home/cohen/cophir/'+line
		root = ET.parse(mydir).getroot()
		photo = root.find('photo')

		# get username and id
		img_id = photo.get('id')
		owner = photo.find('owner')
		username = owner.items()[1][1]

		# get date
		dates = photo.find('dates')
		date_taken = dates.items()[1][1]

		#get tags
		tags = photo.find('tags')
		tag_array = tags.findall('tag')
		all_tags = []
		for tag in tag_array:
			all_tags.append(tag.text)

		#get url
		url = ''
		if photo.find('url') is not None:
			url = photo.find('url').text
		
		#get feature vectors
		mpeg = root.find('Mpeg7')
		multimedia = None
		for elem in mpeg.iter():
			if elem.tag == 'MultimediaContent':
				multimedia = elem
		image = multimedia.find('Image')
		feature_vetors = image.findall('VisualDescriptor')
		descriptors = {}
		for feature_vetor in feature_vetors:
			if feature_vetor.get('type') == 'ScalableColorType':
				descriptors['scalable'] = handle_descriptor(feature_vetor, img_id, 'scalable', ['Coeff'])
			elif feature_vetor.get('type') == 'ColorStructureType':
				descriptors['structure'] = handle_descriptor(feature_vetor, img_id, 'structure', ['Values'])
			elif feature_vetor.get('type') == 'ColorLayoutType':
				descriptors['layout'] = handle_descriptor(feature_vetor, img_id, 'layout',
								['YDCCoeff', 'CbDCCoeff', 'CrDCCoeff', 'YACCoeff5', 'CbACCoeff2', 'CrACCoeff2'])
			elif feature_vetor.get('type') == 'EdgeHistogramType':
				descriptors['histogram'] = handle_descriptor(feature_vetor, img_id, 'histogram', ['BinCounts'])
			elif feature_vetor.get('type') == 'HomogeneousTextureType':
				descriptors['texture'] = handle_descriptor(feature_vetor, img_id, 'texture',
								  ['Average', 'StandardDeviation', 'Energy', 'EnergyDeviation'])

		#build object
		image = { 'user':username, 'id': img_id, 'date': date_taken, 'tags': all_tags, 'url': url, 'descriptors': descriptors}
		str_img = str(username)+';'+str(img_id)+';'+str(date_taken)+';'+str(url)+';'\
				  +str(descriptors['scalable']).replace('[','').replace(']','')+';'\
				  +str(descriptors['structure']).replace('[','').replace(']','')+';'\
				  +str(descriptors['layout']).replace('[','').replace(']','')+';'\
				  +str(descriptors['histogram']).replace('[','').replace(']','')+';'\
				  +str(descriptors['texture']).replace('[','').replace(']','')+';'
		print(str_img)