import threading
import cx_Oracle
import string
import random
import os
import time

RANDOM = 1
my_connector_obj = {
    'username': 'threading',
    'password': 'threaduser',
    'host': 'localhost',
    'sid': 'ORCL'
}
my_connector_str = my_connector_obj['username'] + '/' + my_connector_obj['password']\
                   + '@' + my_connector_obj['host'] + '/' + my_connector_obj['sid']

file = open('mylog.txt', 'a')


class DatabaseThread(threading.Thread):
    def __init__(self, db_conn_object):
        threading.Thread.__init__(self)
        self.db_conn_object = db_conn_object
        self._stop = False
        self.event = threading.Event()
        self.set()

    def set(self):
        self.event.set()

    def clear(self):
        self.event.clear()

    def stop(self):
        self._stop = True

    def run(self):
        while not self._stop:
            self.event.wait()
            try:
                self.db_conn_object.run()
            except Exception as e:
                print(e)


class DummyThreadTableObj:
    def __init__(self):
        self.letters = string.ascii_lowercase
        self.sql = 'INSERT INTO DUMMY_THREAD_TABLE(ATTR1, ATTR2, ATTR3, ATTR4, ATTR5, ATTR6, ATTR7, ' \
                   'ATTR8, ATTR9, ATTR10, ID) VALUES(' + self.generate_random_10attr() + 'dummy_seq.nextval ' \
                   ') RETURNING ID INTO :MY_RETURN'
        self.params = {'MY_RETURN': 0}

    def generate_random_attr(self):
        end_str = ''.join(random.choice(self.letters) for i in range(255))
        return end_str

    def generate_random_10attr(self):
        attr_str = ''
        for i in range(10):
            attr_str += "'" + self.generate_random_attr() + "',"
        return attr_str

    def set_cur_return(self, cursor_var):
        self.params['MY_RETURN'] = cursor_var

    def get_cur_return(self):
        return self.params['MY_RETURN'].getvalue()


class DBInsertObjConn:
    def __init__(self, connector_str, db_object):
        self.connector_str = connector_str
        self.db_object = db_object

    def run(self):
        con = cx_Oracle.connect(self.connector_str, encoding="UTF-8", nencoding="UTF-8")
        cur = con.cursor()
        self.db_object.set_cur_return(cur.var(cx_Oracle.NUMBER))
        cur.execute(self.db_object.sql, self.db_object.params)
        file.write('THREAD DUMMY_TB ' + str(self.db_object.get_cur_return()) + '\n')
        file.flush()
        os.fsync(file)
        time.sleep(60)
        con.commit()
        cur.close()


class DBStuckConnection:
    def __init__(self, connector_str):
        self.connector_str = connector_str

    def run(self):
        cur_list = []
        for i in range(10):
            con = cx_Oracle.connect(self.connector_str, encoding="UTF-8", nencoding="UTF-8")
            cur_list.append(con.cursor())
        file.write('THREAD CONN STUCK\n')
        file.flush()
        time.sleep(60)


class ThreadFactory:
    def __init__(self):
        self._layers = {}

    def register_format(self, layer, creator):
        self._layers[layer] = creator

    def get_layer(self, layer, **kwargs):
        obj = self._layers[layer]
        if obj is None:
            return None
        else:
            return DatabaseThread(obj(**kwargs))


class ThreadWorker:
    def __init__(self, thread_list):
        self.thread_list = thread_list

    def stop_thread(self, thread_id):
        self.thread_list[thread_id].stop()

    def set_thread(self, thread_id):
        self.thread_list[thread_id].set()

    def clear_thread(self, thread_id):
        self.thread_list[thread_id].clear()

    def start_thread(self, thread_id):
        self.thread_list[thread_id].start()

    def stop_all_threads(self):
        [self.stop_thread(item) for item in self.thread_list]

    def set_all_threads(self):
        [self.set_thread(item) for item in self.thread_list]

    def clear_all_threads(self):
        [self.clear_thread(item) for item in self.thread_list]

    def start_all_threads(self):
        [self.start_thread(item) for item in self.thread_list]


# CREATE AND REGISTER FACTORY ELEMENTS
my_factory = ThreadFactory()
my_factory.register_format('DBSTUCK', DBStuckConnection)
my_factory.register_format('DBDUMMY', DBInsertObjConn)

# INIT NEW INSTANCE OF THREADS
db_stuck_param = {'connector_str': my_connector_str}
db_stuck = my_factory.get_layer('DBSTUCK', **db_stuck_param)

dummy_thread_tb_obj = DummyThreadTableObj()
db_dummy_param = {'connector_str': my_connector_str, 'db_object': dummy_thread_tb_obj}
db_dummy = my_factory.get_layer('DBDUMMY', **db_dummy_param)

# INIT THREAD WORKER
thread_list = {
                'DBDUMMY': db_dummy
                 ,
                'DBSTUCK': db_stuck
}
my_thread_worker = ThreadWorker(thread_list)
my_thread_worker.start_all_threads()

print('1 - PAUSE 2 - RESUME 3 - STOP')
var = 1
while var == 1:
    action = int(input())
    if action == 1:
        my_thread_worker.clear_all_threads()
    elif action == 2:
        my_thread_worker.set_all_threads()
    elif action == 3:
        my_thread_worker.stop_all_threads()
        var = 2

#--ALTER SYSTEM KILL SESSION '269, 64338';


#SELECT pid FROM v$process
#WHERE addr =
#(
#    SELECT paddr FROM v$bgprocess
#    WHERE name = 'PMON'
#);
#ORADEBUG WAKEUP 2;