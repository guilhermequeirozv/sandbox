class follow:
    def __init__(self, param):
        self.a = param
        self.serial = None

    def serialize(self):
        return {'a': self.a}

def my_func_needs_lambda(follow, lambda_function):
    return lambda_function(follow)

myfollow = follow(12)
print(my_func_needs_lambda(myfollow,lambda serializable: serializable.serialize()))
