import requests
from array import array
import time


def main():
	try:
		results = {'success': [],'failure': []}
		read_fv_from_hdfs_input = { "limit":"","day":"3","month":"2","descriptors": [ "scalablefv"] }

		read_fv_from_hdfs_rq = requests.post("http://192.168.49.2:31314/read_fv_from_hdfs", json=read_fv_from_hdfs_input)

		read_fv_from_hdfs_output = []
		try:
			read_fv_from_hdfs_output = read_fv_from_hdfs_rq.json()
		except Exception as e:
			print(str(e))
		
		i = -1
		for read_fv_from_hdfs_element in read_fv_from_hdfs_output:
			i = i + 1

			for descriptor in read_fv_from_hdfs_element['descriptors']:
				split_input = read_fv_from_hdfs_element['descriptors'][descriptor].split(',')
				read_fv_from_hdfs_element['descriptors'][descriptor] = [ int(x) for x in split_input ]

			create_feature_files_input = {"img_id":read_fv_from_hdfs_element['img_id'], "descriptors":read_fv_from_hdfs_element['descriptors']}

			create_feature_files_rq = requests.post("http://192.168.49.2:31314/create_feature_files", json=create_feature_files_input)

			files = []
			try:
				files = create_feature_files_rq.json()['files']
			except Exception as e:
				print(str(e))

			for file in files:
				load_feature_file_input = { "img_id": file['img_id'], "filename":file['filename'], "feature_name": file['descriptor'], "feature_dimension": file['dimensions']}
				load_feature_file_rq = requests.post("http://192.168.49.2:31314/load_feature_files", json=load_feature_file_input)

				remove_txt_input = { "img_id": file['img_id'], "filename":file['filename'], "feature_name": file['descriptor'], "feature_dimension": file['dimensions']}
				remove_txt_input_rq = requests.post("http://192.168.49.2:31314/remove_txt", json=remove_txt_input)


	except Exception as e:
		print(str(e))
main()