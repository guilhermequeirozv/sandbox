import boto3
from os import listdir
from os.path import isfile, join


filepath = input("Enter file path")
mybucket = input("Enter bucket name")


onlyfiles = [f for f in listdir(filepath) if isfile(join(filepath, f))]
s3 = boto3.client('s3')
for file in onlyfiles:
	s3.upload_file(filepath+'/'+file,mybucket, file)
