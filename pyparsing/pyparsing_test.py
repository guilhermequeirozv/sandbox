from pyparsing import *
import requests

def execute_command(command):
    if command[0]=='CREATE' or command[0]=='MAKE':
        if command[1] == 'USER':
            response = requests.get('http://127.0.0.1:5000/user/create/'+command[3])
            if response:
                print('response')

a_exp = Optional(oneOf("HI HELLO")).suppress()
b_exp = Optional(oneOf("HOW-R-U")).suppress()
c_exp = Optional(oneOf("PLEASE CAN-YOU DO")).suppress()
f_exp = oneOf("USER") + "IDENTIFIED BY" + Word(alphas)
f_exp_2 = oneOf("POST") + "IDENTIFIED BY" + Word(nums) + ".XML"
g_exp = oneOf("FOLLOWERS POSTS") + "FOR USER" + Word(alphas)
h_exp = "RANDOM" + oneOf("FOLLOWERS USERS POSTS")
e_exp = Word(nums) + (g_exp | h_exp)
d_exp = oneOf("MAKE CREATE") + (e_exp | f_exp | f_exp_2)
i_exp = Optional(oneOf('OK? FOR-ME K')).suppress()
j_exp = Optional(oneOf('WILLYA? PLEASE THANKS TKX')).suppress()
k_exp = Optional('BYE').suppress()
line_parser = a_exp + b_exp + c_exp + d_exp + i_exp + j_exp + k_exp
# line_parser = Word(nums)
parsed = True
while parsed:
    str = input()
    try:
        parsed = line_parser.parseString(str)
        execute_command(parsed)
    except Exception as e:
        print(e)
        parsed = False
