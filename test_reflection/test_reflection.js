c_type_properties = {'userInfo':1,'currentUser':1,'images':2,
                     'posts':2,'followInformation':2,
                     'comments':2,'following':3, 'followers':3
                    };

function loadTemplateByType(type){
    console.log(type)
    if(type === 'currentUser'){
        return {userInfo:'userInfo',images:'images',posts:'posts',followInformation:'followInformation'};
    }else if(type === 'userInfo'){
        return {username:'string',profileImg:'string'}; 
    }else if(type === 'profileInfo'){
        return { userInfo:'userInfo',images:'images',npubs:'number',followMe:'number',followInformation:'followInformation'}
    }else if(type === 'images'){
        return {path:'string',img_id:'number'};
    }else if(type === 'posts'){
        return {userInfo:'userInfo',image:'string',subtitle:'string',time:'string',heart:'boolean',likes:'number',comments:'comments'};
    }else if(type === 'followInformation'){
        return {nfollowing:'number',nfollowers:'number',following:'following',followers:'followers'};
    }else if(type === 'following' || type === 'followers'){
        return {username:'string',profileImg:'string',following:'number'}; 
    }else if(type === 'comments'){
        return {username:'string',text:'string',heart:'boolean',time:'string',likes:'number',owner:'boolean'};
    }
}
function genericValidate(object,type){
    let template = loadTemplateByType(type);
    Object.getOwnPropertyNames(template).forEach(function(val,idx,array){
        console.log('evaluating: ' + val + ' of type ' + template[val]);
        if(!object.hasOwnProperty(val)){
            console.log("objects " + type + " are incompatibles.. missing " + val + " property");
            return;
        }else if(c_type_properties[val]==1){//property is a type
            console.log('property is a type');
            genericValidate(object[val],val);
        }else if (c_type_properties[val]==2){//property is an array
            console.log('property is an array');
            if(Array.isArray(object[val])){
                for(let i = 0; i < object[val].length; i++){
                    genericValidate(object[val][i],template[val]);
                }
            }else{
                console.log("objects properties are incompatibles.. type is " + typeof object[val] + " expected: Array");
                return;
            }
        }else if(c_type_properties[val]==3){//property is a dictionary
            Object.keys(object[val]).forEach(function(key){
                genericValidate(object[val][key],val);
            });
        }else if(typeof object[val] != template[val]){
            console.log("objects properties are incompatibles.. type is " + typeof object[val] + " expected: " + template[val]);
            return;
        }
    });
    console.log('objects are compatible');
}
